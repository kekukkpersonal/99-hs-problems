import Data.Function
import Data.List
import Data.Ord

-- Eksam aines Funktsionaalne programmeerimine
-- Eksaminand: Kert Kukk

-- 1 
sumDigEven :: String -> Int
sumDigEven (a:as) = undefined

isInt i = i == fromInteger (round i)

-- 2
primes :: [Integer]
primes = sieve [2..]
  where
    sieve (p:xs) = p : sieve [x|x <- xs, x `mod` p > 0]

primeToDuple xs = [((esimene), (teine))] ++ primeToDuple (drop 2 xs)
   where
      esimene = head xs
      teine = last(take 2 xs)

primeStream :: [(Integer,Integer)]
primeStream = primeToDuple primes

-- 3
llsort :: [[a]] -> [[a]]
llsort = sortBy (comparing length)

-- 4 
act :: (b -> d -> c) -> (a -> b) -> (a -> b -> d) -> a -> c
act f1 f2 f3 a = f1 b (f3 a b)
   where b = f2 a

-- 5 
data Tree a = Leaf a | LeafEmpty | Branch a (Tree a) [Tree a] | Node [a] (Tree a) (Maybe (Tree a)) deriving (Show)

puu = Leaf 2

findElem LeafEmpty x = -1
findElem (Leaf a) x
   |a == x = x
   |otherwise = -1
findElem (Branch a l r) x
   |a == x = x
   |findElem l x == x = x
   |listFind r x == x = x
   |otherwise = -1
findElem (Node as l r) x 
   |elem x as = x
   |findElem l x == x = x
   

listFind (a:as) x
   |findElem a x == x = x
   |otherwise = listFind as x
-- (Maybe (Tree a)) osa ei oska

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f LeafEmpty = LeafEmpty
mapTree f (Leaf a) = Leaf (f a)
mapTree f (Branch a tree trees) = Branch (f a) (mapTree f tree) (mapTreeList f trees)
   where mapTreeList f (t:ts) = [(mapTree f t)] ++ mapTreeList f ts
-- (Maybe (Tree a)) osa ei oska
--mapTree f (Node as tree maybeTree) = Node (map f as) (maptree f tree) (fmap f maybeTree)



-- 6
exitio :: IO ()
exitio = do
   putStr "input: "
   l <- getLine
   if "exit" `isInfixOf` l
      then putStrLn "bye bye." 
      else exitio


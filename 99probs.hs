-- NOTES --
-- ! -> ühksik hüüumärk, tähendab, et element on 'strict', ehk element peab omama väärtust.
-- (!!) -> (!!) :: [a] -> Int -> a Võetakse list ja tema indeks ja tagastatakse indeksi all olev element (indeksid algavad nullist). nt: [1,2] !! 0 tagastab 1'e
-- 'mod' ja 'rem' -> jääk 2. -Nt: 2 `mod` (-3)  ==  -1 või 2 `rem` (-3)  ==  2
-- from... -> fromIntegral :: (Integral a, Num b) => a -> b Nt: 4 / fromIntegral (length [1,2,3]) tagastab 1.33

-- Problem 1
-- Prelude> myLast [1,2,3,4]
-- 4
myLast :: [a] -> a
myLast [] = error "Can't find last element from empty list"
myLast [a] = a
myLast (_:xs) = myLast xs
-- või myLast as = head (reverse as)

-- Problem 2
-- Prelude> myButLast [1,2,3,4]
-- 3
myButLast :: [a] -> a
myButLast [] = error "not enough elements"
myButLast [a] = error "not enough elements"
myButLast [a,b] = a
myButLast (_:as) = myButLast as
-- või myButLast = last . init
-- või myButLast = head . tail . reverse

-- Problem 3
-- Prelude> elementAt [1,2,3] 2
-- 2
elementAt :: [a] -> Int -> a
elementAt [] _ = error "Can't find element from empty list"
elementAt as i
   |i > length as = error "element index can't be higher than list length"
   |i < 1 = error "can't find element with index lesser than 1"
   |otherwise = head $ drop (i - 1) as
-- või isPalindrome' xs  = (head xs) == (last xs) && (isPalindrome' $ init $ tail xs)

-- Problem 4
-- Prelude> myLength [123, 456, 789]
-- 3
myLength :: [a] -> Int
myLength [] = 0
myLength [a] = 1
myLength (a:as) = 1 + myLength as
-- või myLength = sum . map (\_->1)

-- Problem 5
-- Prelude> myReverse [1,2,3,4]
-- [4,3,2,1]
myReverse :: [a] -> [a]
myReverse [] = []
myReverse [a] = [a]
myReverse (a:as) = myReverse as ++ [a]


-- Problem 6
-- *Main> isPalindrome [1,2,3]
-- False
-- *Main> isPalindrome "madamimadam"
-- True
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome as
   |length as < 3 = False
   |length as `mod` 2 == 0 = False
   |firstHalf == secondHalf = True
   |otherwise = False
   where firstHalf = take halfMinusOne as
         secondHalf = take halfMinusOne (reverse as)
         halfMinusOne = fromEnum ((fromIntegral (length as) - 1) / 2)

-- Problem 7
-- *Main> flatten (Elem 5)
-- [5]
-- *Main> flatten (List [Elem 1, List [Elem 2, List [Elem 3, Elem 4], Elem 5]])
-- [1,2,3,4,5]
-- *Main> flatten (List [])
-- []
data NestedList a = Elem a | List  [NestedList a] deriving (Show)

flatten :: NestedList a -> [a]
flatten (Elem a) = [a]
flatten (List []) = []
flatten (List (x:xs)) = flatten x ++ flatten (List xs)
--flatten 
--flatten  NestedList a] = [a]

-- Problem 8
-- compress "aaaabccaadeeee"
-- "abcade"
compress :: String -> String
compress [] = []
compress (x:xs) = x:compress xs'
   where xs' = (dropWhile (==x) xs)

-- Problem 9
-- pack ['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e']
-- ["aaaa","b","cc","aa","d","eeee"]
pack :: [Char] -> [String]
pack [] = []
pack (x:xs) = [x:xs'] ++ pack xs''
   where xs' = takeWhile (== x) xs
         xs'' = dropWhile (== x) xs

-- Problem 10
-- encode "aaaabccaadeeee"
-- [(4,'a'),(1,'b'),(2,'c'),(2,'a'),(1,'d'),(4,'e')]
encode :: String -> [(Int, Char)]
encode [] = []
encode (x:xs) = (1 + length xs', x) : encode xs''
   where xs' = takeWhile (== x) xs
         xs'' = dropWhile (== x) xs

-- Problem 11
-- P11> encodeModified "aaaabccaadeeee"
-- [Multiple 4 'a',Single 'b',Multiple 2 'c',
-- Multiple 2 'a',Single 'd',Multiple 4 'e']
data ListItem a = Single a | Multiple Int a deriving (Show)

encodeModified :: (Eq a) => [a] -> [ListItem a] 
encodeModified [] = []
encodeModified [a] = [Single a]
encodeModified all@(a:as)
   |elemsLength > 1 = [Multiple elemsLength a] ++ encodeModified (drop elemsLength all)
   |otherwise = [Single a] ++ encodeModified as
   where elemsLength = length $ takeWhile (== a) all

-- Problem 12
-- P12> decodeModified 
--        [Multiple 4 'a',Single 'b',Multiple 2 'c',
--         Multiple 2 'a',Single 'd',Multiple 4 'e']
-- "aaaabccaadeeee"
decodeModified :: [ListItem a] -> [a]
decodeModified [(Single a)] = [a]
decodeModified [Multiple i a] = replicate i a
decodeModified (a:as) = decodeModified [a] ++ decodeModified as

-- Problem 13
-- Sama mis 11 ainult teistsugune lahendus. Ei puutu minu skoopi

-- Problem 14
-- dupli [1, 2, 3]
-- [1,1,2,2,3,3]
dupli :: [Int] -> [Int]
dupli [] = []
dupli (a:as) = replicate a a ++ dupli as

-- Problem 15
-- repli "abc" 3
-- "aaabbbccc"
repli :: [a] -> Int -> [a]
repli [] _ = []
repli (a:as) i = replicate i a ++ repli as i

-- Problem 16
-- dropEvery "abcdefghik" 3
-- "abdeghk"
dropEvery :: [a] -> Int -> [a]
dropEvery [] _ = []
dropEvery all i = take (i - 1) all ++ dropEvery (drop i all) i

-- Problem 17
-- split "abcdefghik" 3
-- ("abc", "defghik")
split :: [a] -> Int -> ([a], [a])
split [] _ = ([],[])
split all i = undefined
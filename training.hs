import Data.Function(on)
import Data.List
-- ainult kindlta funktsiooni import:
-- import Data.Function(on)
-- import Data.List(sortBy)

ll = [1,2,3,4,5]
-- 1
-- Prelude> myLast [1,2,3,4]
-- 4
myLast :: [a] -> a
myLast [] = error "empty list"
myLast [a] = a
myLast (a:as) = myLast as
-- reverse . head

-- 2
-- Prelude> myButLast [1,2,3,4]
-- 3
myButLast :: [a] -> a
myButLast = head . tail . reverse


-- 3
-- Prelude> elementAt [1,2,3] 2
-- 2
elementAt :: [a] -> Int -> a
elementAt [] _ = error "empty list"
elementAt all@(a:as) x 
   |x <= length all = last $ take x all
   |x > length all = error "index out of bounds"
   |otherwise = error "list isn't large enough"
   
-- 4
-- Prelude> myLength [123, 456, 789]
-- 3
myLength :: [a] -> Int
myLength [] = 0
myLength [a] = 1
myLength (a:as) = 1 + myLength as

--5 
-- Prelude> myReverse [1,2,3,4]
-- [4,3,2,1]
myReverse :: [a] -> [a]
myReverse [] = []
myReverse [a] = [a]
myReverse (a:as) = myReverse as ++ [a] 

-- 6 
-- *Main> isPalindrome [1,2,4,8,16,8,4,2,1]
-- True
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome all@(a:as)
   |isOdd /= 1 = False
   |firstHalf == reverse(secondHalf) = True
   |otherwise = False
   where
      isOdd = (mod (length all) 2)
      halfMinusOne = round(fromIntegral((length all) - 1) / 2) -- round on üleliigne
      firstHalf = take halfMinusOne all
      secondHalf = drop (halfMinusOne + 1) all
      
--7 
-- *Main> flatten (List [Elem 1, List [Elem 2, List [Elem 3, Elem 4], Elem 5]])
-- [1,2,3,4,5]
data NestedList a = Elem a | List [NestedList a] deriving (Show)

testNestedList = List [Elem 1, List [Elem 2, Elem 3], Elem 4]

flatten :: NestedList a -> [a]
flatten (List []) = []
flatten (Elem a) = [a]
flatten (List (a:as)) = flatten a ++ flatten (List as)

-- 8
-- compress "aaaabccaadeeee"
-- "abcade"
compress :: (Eq a) => [a] -> [a]
compress [] = []
compress all@(a:as) = [a] ++ compress (dropWhile (== a) all)

-- 9
-- pack ['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e']
-- ["aaaa","b","cc","aa","d","eeee"]
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack all@(a:as) = [replication] ++ pack droppedList
   where
      rowLength = length(takeWhile (== a) all)
      replication = replicate rowLength a
      droppedList = dropWhile (== a) all
      
-- 10 
-- encode "aaaabccaadeeee"
-- [(4,'a'),(1,'b'),(2,'c'),(2,'a'),(1,'d'),(4,'e')]
encode :: (Eq a) => [a] -> [(Int, a)]
encode [] = []
encode all@(a:as) = [(rowLength, a)] ++ encode(drop rowLength all)
   where
      rowLength = length(takeWhile (== a) all)

-- 11
-- encodeModified "aaaabccaadeeee"
-- [Multiple 4 'a',Single 'b',Multiple 2 'c', Multiple 2 'a',Single 'd',Multiple 4 'e']

data Element a = Multiple Int a | Single a deriving (Show)

-- test = Multiple 3 'a'
-- test1 = Single 'b'

encodeModified :: (Eq a) => [a] -> [Element a]
encodeModified [] = []
encodeModified all@(a:as)
   |fst encHead > 1 = [Multiple (fst encHead) (snd encHead)] ++ encodeModified dropWhileA
   |otherwise = [Single a] ++ encodeModified dropWhileA
      where
         encoded = encode all
         encHead = head encoded
         dropWhileA = dropWhile (== a) all

-- 12
-- decodeModified [Multiple 4 'a',Single 'b',Multiple 2 'c', Multiple 2 'a',Single 'd',Multiple 4 'e']
-- "aaaabccaadeeee"

decodeModified :: [Element a] -> [a]
decodeModified [] = []
decodeModified [(Single a)] = [a]
decodeModified [(Multiple int a)] = replicate int a
decodeModified (a:as) = decodeModified [a] ++ decodeModified as

-- 13
-- Sama mis 11. Skoobist väljas.

-- 14
-- dupli [1, 2, 3]
-- [1,1,2,2,3,3]
dupli :: [a] -> [a]
dupli [] = []
dupli (a:as) = a:a:dupli as

-- 15
-- repli "abc" 3
-- "aaabbbccc"
repli :: [a] -> Int -> [a]
repli [] _ = []
repli (a:as) i = replicate i a ++ repli as i

--16
-- dropEvery "abcdefghik" 3
-- "abdeghk"
dropEvery :: [a] -> Int -> [a]
dropEvery [] _ = []
dropEvery all@(a:as) i = take (i-1) all ++ dropEvery (drop i all) i

-- 17
-- split "abcdefghik" 3
-- ("abc", "defghik")
split :: [a] -> Int -> ([a], [a])
split _ 0 = error "splitter must be creater than zero"
split [] _ = ([],[])
split all i = (take i all, drop i all)


-- 18
-- slice ['a','b','c','d','e','f','g','h','i','k'] 3 7
-- "cdefg"
slice :: [a] -> Int -> Int -> [a]
slice [] _ _ = []
slice all i j
   |i > j = error "first index argument must be smaller than second arg"
   |otherwise = drop (i - 1) $ take j all
   
-- 19
-- rotate ['a','b','c','d','e','f','g','h'] 3
-- "defghabc"
-- rotate ['a','b','c','d','e','f','g','h'] (-2)
--"ghabcdef"
rotate :: [a] -> Int -> [a]
rotate [] _ = []
rotate all@(a:as) i
   |i < 0 = (reverse(take (abs(i)) (reverse all))) ++ (reverse(drop (abs(i)) (reverse all)))
   |i > 0 = (drop i all) ++ (take i all)
   |otherwise = all

-- 20
-- removeAt 2 "abcd"
-- ('b',"acd")
removeAt :: Int -> [a] -> (a, [a])
removeAt _ [] = error "can't remove from empty list"
removeAt i all = ((last(take i all)), (take (i-1) all) ++ (drop (i) all))

--   __________________
--  /                  \
-- <  EKSAMI ÜLESANDED  >
--  \__________________/

-- EKSAM I

-- 1
groupEq :: (Eq a) => [a] -> [[a]]
groupEq [] = []
groupEq [a] = [[a]]
groupEq all@(a:as) = [(takeWhile (== a) all)] ++ groupEq (dropWhile (== a) all)

-- 2
primeStream :: [Integer]
primeStream = second $ sieve [2..20]
   where
      sieve [] = []
      sieve (p:ps) = p : sieve [x | x <- ps, mod x p > 0]
      second [] = []
      second (x:xs) = (head xs) : second (tail xs)

-- 3
data Shape = Ruut Double | Triangle Double Double Double

instance Show Shape
   where
      show (Ruut a) = "Ruut : " ++ show a
      show (Triangle a b c)
         |isTriangle (Triangle a b c) = "Kolmnurk: (" ++ show a ++ ", " ++ show b ++ ", " ++ show c ++ ")"
         |a + b > c = "vale kolmnurk: " ++ show a ++ " + " ++ show b ++ " > " ++ show c
         |a + c > b = "vale kolmnurk: " ++ show a ++ " + " ++ show c ++ " > " ++ show b
         |b + c > a = "vale komlnurk: " ++ show b ++ " + " ++ show c ++ " > " ++ show a
         
area :: Shape -> Double
area (Ruut a) = a * a
area (Triangle a b c) = sqrt(s * (s - a) * (s - b) * (s - c))
   where s = (a + b + c) / 2

isTriangle :: Shape -> Bool
isTriangle (Ruut _) = False
isTriangle (Triangle a b c) = a + b > c && a + c > b && b + c > a

-- 4
act :: (a -> b -> c) -> (a -> b) -> a -> a -> c
act f1 f2 a1 a2 = f1 a1 (f2 a2)

-- 5
expToLin :: Integer -> Integer
expToLin 0 = 0
expToLin 1 = 10
expToLin 2 = 110
expToLin n = expToLin (n-2) + expToLin (n-3) + 100

-- expToLin function implemented in linear manner
expToLinDone :: Integer -> Integer
expToLinDone n = exps !! (fromIntegral n)
   where
      exps = 0 : 10 : 110 : map (+100) (zipWith (+) exps (tail exps)) --[0,10,110] ++
      
-- 6
data Tree a = Leaf a | LeafEmpty | Node1 a (Tree a) | Node2 a (Tree a) (Tree a) deriving (Show)

testPuu = Node1 1 (Node2 2 (Node2 3 (LeafEmpty) (Leaf 4)) (Leaf 3))

findElem LeafEmpty x = -1
findElem (Leaf a) x
   |a == x = a
   |otherwise = -1
findElem (Node1 a tree) x
   |a == x = a
   |otherwise = findElem (tree) x
findElem (Node2 a l r) x
   |a == x = a
   |findElem r x == x = x
   |findElem l x == x = x
   |otherwise = -1
   
height (Leaf _) = 1
height (LeafEmpty) = 0
height (Node1 _ tree) = 1 + height tree
height (Node2 _ l r) = 1 + max (height l) (height r)

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f LeafEmpty = LeafEmpty
mapTree f (Leaf a) = Leaf (f a)
mapTree f (Node1 a tree) = Node1 (f a) (mapTree f tree)
mapTree f (Node2 a l r) = Node2 (f a) (mapTree f l) (mapTree f r)

-- 7
guess :: IO ()
guess = do
   putStr "guess: "
   arv <- getLine
   if arv /= "42"
      then guessAgain
      else putStrLn "You won, bye bye."

guessAgain :: IO ()
guessAgain = do
   putStr "nope, try again: "
   arv <- getLine
   if arv /= "42"
      then guessAgain
      else putStrLn "You won, bye bye."


-- EKSAM II

-- 1
d :: [Int] -> [Int]
d [] = []
d (a:as) = (replicate a a) ++ (d as)

-- 2
--b :: [String]
b = concat $ iterate ((:) <$> "01" <*>) [[]]

-- take 10 b2
-- see on arusaadav lahendusest b
b2 = [] : go b2 
   where
      go (x:xs) = ('0':x) : ('1':x) : go xs

-- lisa enesearenduseks. Jääb tsükklisse :(
-- c i all = [x | x <- all, length x == i]

-- 3

data Loom = Koer String | Kass String | Varblane String | Kuldnokk String deriving (Show, Eq)

-- custom Eq
-- instance Eq Loom where
--    (Koer n1) == (Koer n2) = n1 == n2
--    (Kass n1) == (Kass n2) = n1 == n2
--    (Varblane n1) == (Varblane n2) = n1 == n2
--    (Kuldnokk n1) == (Kuldnokk n2) = n1 == n2

-- custom Ord. Sellest piisaks, et realiseerida sortAnimals, aga see on liiga kohmakas. All lühem versioon
-- instance Ord Loom where
--    compare (Koer n1) (Koer n2) = compare n1 n2
--    compare (Koer n1) (Kass n2) = compare n1 n2
--    compare (Koer n1) (Varblane n2) = compare n1 n2
--    compare (Koer n1) (Kuldnokk n2) = compare n1 n2
--    compare (Kass n1) (Koer n2) = compare n1 n2
--    compare (Kass n1) (Kass n2) = compare n1 n2
--    compare (Kass n1) (Varblane n2) = compare n1 n2
--    compare (Kass n1) (Kuldnokk n2) = compare n1 n2
--    compare (Varblane n1) (Koer n2) = compare n1 n2
--    compare (Varblane n1) (Kass n2) = compare n1 n2
--    compare (Varblane n1) (Varblane n2) = compare n1 n2
--    compare (Varblane n1) (Kuldnokk n2) = compare n1 n2
--    compare (Kuldnokk n1) (Koer n2) = compare n1 n2
--    compare (Kuldnokk n1) (Kass n2) = compare n1 n2
--    compare (Kuldnokk n1) (Varblane n2) = compare n1 n2
--    compare (Kuldnokk n1) (Kuldnokk n2) = compare n1 n2

testAnimalList = [(Koer "Mare"), (Kass "Alvi"), (Varblane "Xander"), (Kuldnokk "Iiah")]

animalName :: Loom -> String
animalName (Koer a) = a
animalName (Kass a) = a
animalName (Varblane a) = a
animalName (Kuldnokk a) = a

isMammal :: Loom -> Bool
isMammal (Koer _) = True
isMammal (Kass _) = True
isMammal (Varblane _) = False
isMammal (Kuldnokk _) = False

sortAnimals :: [Loom] -> [Loom]
sortAnimals = sortBy (on compare animalName)

-- 4
actAgain :: ((v -> a -> b) -> c -> b -> d) -> ((c -> b -> d) -> q) -> (a -> b) -> q
actAgain a1 a2 a3 = undefined


-- 5
-- exponential complexity
c :: Integer -> Integer
c n 
   | n <= 4 = n + 10
   | otherwise = c (n-1) + 2 * (c (n-4))

-- linear complexity
c2 :: Integer -> Integer
c2 n
  | n <= 4 = n + 10
  | otherwise = results !! fromInteger (n - 1)
  where
    results = [11..14] ++ zipWith f (drop 3 results) results 
    f a b = a + 2*b

-- 7

   

-- KORDUSEKSAM

-- 1
a :: [Int] -> [[Int]]
a []  = [[]]
a (x:xs) = a xs ++ map (x:) (a xs)

-- 2
testDiag = [[1,0,0],[0,2,0],[0,0,3]]

diagonalVal :: [[a]] -> [a]
diagonalVal [] = []
diagonalVal all@(a:as) = [(nthElem val a)] ++ diagonalVal as
   where
      val = length all

nthElem :: Int -> [a] -> a
nthElem i all = head(drop (i-1) all)

-- 3
data Vars = VarA | VarB | VarC | VarD | VarE deriving (Show, Eq, Ord)

testVarsList = [VarB, VarC, VarA]


quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = quicksort [y | y <- xs, y <= x] ++ [x] ++ quicksort [y | y <- xs, y > x]

sortVars :: [Vars] -> [Vars]
sortVars vs = quicksort vs
-- või kui kasutada Data.List'i siis lihtsalt sort quicksort'i asemel

-- 4
-- http://umairsaeed.com/blog/2015/07/08/pascals-triangle/

pascal :: [[Int]]
pascal = iterate (\row -> zipWith (+) ([0] ++ row) (row ++ [0])) [1]

-- sama mis pascal
-- next xs = zipWith (+) ([0] ++ xs) (xs ++ [0])
-- pascalAgain = iterate next [1]



-- MISC

fibs = 0:1:zipWith (+) fibs (tail fibs)

func :: (a -> b) -> (b -> a -> c -> d) -> a -> ((p -> p) -> c) -> d
func g h a q = h (g a) a (q id)

func2 :: ((a -> b -> c) -> q) -> c -> q
func2 x c = x fn
   where fn a b = c
   

subsetsOfSize :: Int -> [a] -> [[a]]
subsetsOfSize 0 _ = [[]]
subsetsOfSize _ [] = []
subsetsOfSize k (x:xs) = map (x:) (subsetsOfSize (k - 1) xs) ++ subsetsOfSize k xs


myMap :: (a -> b) -> [a] -> [b]
myMap f [] = []
myMap f (x:xs) = f x : myMap f xs

greet :: IO ()
greet = do
   putStrLn "Mis on su nimi?"
   nimi <- getLine
   putStrLn ("Tere " ++  nimi ++ "!")
   



















